# -*- coding: utf-8 -*-
import requests
import json
import datetime
try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO

class MercadoLibre:

    error = False
    message = False
    access_token = False
    refresh_token = False

    def __init__(self, app_id, app_secret, cert):
        self.app_id = app_id.strip()
        self.app_secret = app_secret.strip()
        self.cert = cert
        print("Se solicita un access token")

        if not self.access_token:
            self.generate_access_token()

        # Setea el id
        if not self.set_seller_id():
            Exception('Cannot set seller id because {0}'.format(self.error))

    # Setea datos de usuario y seller id
    def set_seller_id(self):
        url = 'https://api.mercadolibre.com/users/me?access_token={token}'.format(
            token=self.access_token
        )

        req = requests.get(url, verify=self.cert)
        res = req.json()
        if 'error' in res:
            if 'cause' in res and len(res['cause']) > 0:
                self.error = res['cause'][0]['code']
                self.message = res['cause'][0]['message']
            else:
                self.error = res['error']
                self.message = res['message']
            self.seller_id = False
            return False

        self.seller_id = res['id']
        self.user_data = res

    # Genera el access token para permitir realizar acciones en mercadolibre
    def generate_access_token(self):
        url = 'https://api.mercadolibre.com/oauth/token'
        payload = {
            'grant_type': 'client_credentials',
            'client_id': self.app_id,
            'client_secret': self.app_secret
        }

        req = requests.post(url, payload, verify=self.cert)
        res = req.json()

        if 'error' in res and res['error']:
            if 'cause' in res and len(res['cause']) > 0:
                self.error = res['cause'][0]['code']
                self.message = res['cause'][0]['message']
            else:
                self.error = res['error']
                self.message = res['message']
            return False

        self.access_token = res['access_token']
        self.refresh_token = res['refresh_token']

        return True

    # Actualiza token de mercadolibre con un refresh token
    def refresh_access_token(self):
        url = 'https://api.mercadolibre.com/oauth/token'
        payload = {
            'grant_type': 'refresh_token',
            'client_id': self.app_id,
            'client_secret': self.app_secret,
            'refresh_token': self.refresh_token
        }

        req = requests.post(url, payload, verify=self.cert)
        res = req.json()

        if 'error' in res:
            if 'cause' in res and len(res['cause']) > 0:
                self.error = res['cause'][0]['code']
                self.message = res['cause'][0]['message']
            else:
                self.error = res['error']
                self.message = res['message']
            return False

        self.access_token = res['access_token']
        self.refresh_token = res['refresh_token']
        return True

    # Cambia stock de un producto en mercadolibre
    def change_stock(self, product_id, new_stock, variant_id=False):
        if variant_id:
            variation = 'variations/{0}'.format(variant_id)
        else:
            variation = ''

        url = 'https://api.mercadolibre.com/items/{product_id}/{variation}?access_token={token}'.format(
            token=self.access_token,
            product_id=product_id,
            variation=variation
        )

        if not variant_id:
            if new_stock <= 0:
                return self.pause_publication(product_id)
            else:
                data = {
                    'available_quantity': int(new_stock)
                }
                self.resume_publication(product_id)
        else:
            if new_stock <= 0:
                data = {
                    'available_quantity': 0
                }
            else:
                data = {
                    'available_quantity': int(new_stock)
                }

        payload = StringIO()
        json.dump(data, payload)

        req = requests.put(url, data=payload.getvalue(), verify=self.cert)
        res = req.json()

        if 'error' in res:
            try:
                if 'cause' in res and len(res['cause']) > 0:
                    self.error = res['cause'][0]['code']
                    self.message = res['cause'][0]['message']
                else:
                    self.error = res['error']
                    self.message = res['message']
            except:
                print(res)
                self.error = res['error']
                self.message = res['message']

            print(self.message)
            return False
        else:
            if variant_id:
                print('Stock actualizado del producto {0} con variante {1} a {2}'.format(product_id, variant_id, new_stock))
            else:
                print('Stock actualizado del producto {0} a {1}'.format(product_id, new_stock))

            return True

    # Pausa publicacion en mercadolibre
    def pause_publication(self, product_id):
        url = 'https://api.mercadolibre.com/items/{product_id}?access_token={token}'.format(
            token=self.access_token,
            product_id=product_id
        )

        data = {
            'status': 'paused'
        }
        ml_product = self.get_product(product_id)
        if ml_product and ml_product['status'] != 'active':
            return

        payload = StringIO()
        json.dump(data, payload)

        req = requests.put(url, data=payload.getvalue(), verify=self.cert)
        res = req.json()

        if 'error' in res:
            if 'cause' in res and len(res['cause']) > 0:
                self.error = res['cause'][0]['code']
                self.message = res['cause'][0]['message']
            else:
                self.error = res['error']
                self.message = res['message']
        else:
            print('La publiaccion {0} paso a estado pausado'.format(product_id))

            return True

    # Reactiva publicacion en mercadolibre
    def resume_publication(self, product_id):
        url = 'https://api.mercadolibre.com/items/{product_id}?access_token={token}'.format(
            token=self.access_token,
            product_id=product_id
        )
        ml_product = self.get_product(product_id)
        if ml_product and ml_product['status'] == 'active':
            return
        data = {
            'status': 'active'
        }

        payload = StringIO()
        json.dump(data, payload)

        req = requests.put(url, data=payload.getvalue(), verify=self.cert)
        res = req.json()

        if 'error' in res:
            if 'cause' in res and len(res['cause']) > 0:
                self.error = res['cause'][0]['code']
                self.message = res['cause'][0]['message']
            else:
                self.error = res['error']
                self.message = res['message']
            return False
        else:
            print('Publicacion {0} se cambio el estado a activo'.format(product_id))

            return True

    # Cambia precio en un producto o producto variante en mercadolibre
    def change_price(self, product_id, new_price, variant_id=False):
        if isinstance(variant_id, list):
            url = 'https://api.mercadolibre.com/items/{product_id}/?access_token={token}'.format(
                token=self.access_token,
                product_id=product_id,
            )
            data = {'variations': []}
            for var in variant_id:
                data['variations'].append({
                    'id': var['id'],
                    'price': new_price
                })

        else:
            if variant_id:
                variation = 'variations/{0}'.format(variant_id)
            else:
                variation = ''

            url = 'https://api.mercadolibre.com/items/{product_id}/{variation}?access_token={token}'.format(
                token=self.access_token,
                product_id=product_id,
                variation=variation
            )

            data = {
                'price': float(new_price)
            }

        payload = StringIO()
        json.dump(data, payload)

        req = requests.put(url, data=payload.getvalue(), verify=self.cert)
        res = req.json()

        if 'error' in res:
            if 'cause' in res and len(res['cause']) > 0:
                self.error = res['cause'][0]['code']
                self.message = res['cause'][0]['message']
                if isinstance(variant_id, list) or not variant_id:
                    print('Fallo la sincronizacion del producto {id}: {error} {cause}'.format(id=product_id,
                                                                                              error=res['error'],
                                                                                              cause=res['cause'][0]['message']))
            else:
                self.error = res['error']
                self.message = res['message']
                if isinstance(variant_id, list) or not variant_id:
                    print('Fallo la sincronizacion del producto {id}: {error} {cause}'.format(id=product_id,
                                                                                              error=res['error'],
                                                                                              cause=res['message']))
            return False
        else:
            print('Precio actualizado del producto {0} a ${1}'.format(product_id, new_price))
            return True

    # Trae el producto en mercadolibre usando el codigo MLA
    def get_product(self, product_id, re=False):
        url = 'https://api.mercadolibre.com/items/{product_id}?access_token={token}'.format(
            token=self.access_token,
            product_id=product_id
        )

        req = requests.get(url, verify=self.cert)
        res = req.json()
        if 'error' in res:
            if 'cause' in res and len(res['cause']) > 0:
                self.error = res['cause'][0]['code']
                self.message = res['cause'][0]['message']
            else:
                self.error = res['error']
                self.message = res['message']
            return False
        else:
            return res

    # Confirma si las credenciales funcionan
    def check_session(self):
        url = 'https://api.mercadolibre.com/users/me?access_token={token}'.format(
            token=self.access_token
        )

        req = requests.get(url, verify=self.cert)
        res = req.json()
        if 'error' in res:
            if 'cause' in res and len(res['cause']) > 0:
                self.error = res['cause'][0]['code']
                self.message = res['cause'][0]['message']
            else:
                self.error = res['error']
                self.message = res['message']
            return False
        else:
            return True

    # Obtiene un producto tipo variante
    def get_product_variation(self, product_id, variant_id):
        url = 'https://api.mercadolibre.com/items/{product_id}/variations?access_token={token}'.format(
            token=self.access_token,
            product_id=product_id
        )

        req = requests.get(url, verify=self.cert)
        res = req.json()
        variant_searched = False

        for variant in res:
            if int(variant['id']) == int(variant_id):
                variant_searched = variant
                break

        if 'error' in res:
            if 'cause' in res and len(res['cause']) > 0:
                self.error = res['cause'][0]['code']
                self.message = res['cause'][0]['message']
            else:
                self.error = res['error']
                self.message = res['message']
            return False
        elif not variant_searched:
            return False
        else:
            return variant_searched

    # Trae las ordenes filtradas en mercadolibre
    def get_orders(self):
        date = datetime.datetime.now()
        if date.month == 1:
            last_month = "{year}-{month}-{day}".format(year=date.year - 1, month=11, day=date.day)
        else:
            last_month = "{year}-{month}-{day}".format(year=date.year, month=date.month - 1, day=date.day)

        url = 'https://api.mercadolibre.com/orders/search?seller={seller_id}&access_token={token}&order.date_created.from={date_from}T00:00:00.000-00:00'.format(
            token=self.access_token,
            seller_id=self.seller_id,
            date_from=last_month
        )

        req = requests.get(url, verify=self.cert)
        res = req.json()

        if 'error' in res:
            if 'cause' in res and len(res['cause']) > 0:
                self.error = res['cause'][0]['code']
                self.message = res['cause'][0]['message']
            else:
                self.error = res['error']
                self.message = res['message']
            return False
        else:
            return res['results']

    # Trae informacion de un usuario especifico
    def get_user_informaton(self, user_id):
        url_info = 'https://api.mercadolibre.com/orders/{user_id}?access_token={token}'.format(
            token=self.access_token,
            user_id=user_id
        )

        req = requests.get(url_info, verify=self.cert)
        info = req.json()
        if 'error' in info:
            if 'cause' in info and len(info['cause']) > 0:
                self.error = info['cause'][0]['error']
                self.message = info['cause'][0]['message']
            else:
                self.error = info['error']
                self.message = info['message']
            return False
        else:
            return info

    # Solicita a mercadolibre la generacion de un usuario de prueba.
    def generate_test_user(self):
        url = 'https://api.mercadolibre.com/users/test_user?access_token={token}'.format(
            token=self.access_token
        )

        req = requests.post(url, '{"site_id":"MLA"}', verify=self.cert, json=True)
        res = req.json()

        if 'error' in res:
            if 'cause' in res and len(res['cause']) > 0:
                self.error = res['cause'][0]['error']
                self.message = res['cause'][0]['message']
            else:
                self.error = res['error']
                self.message = res['message']
            return False
        else:
            return res