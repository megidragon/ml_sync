# -*- coding: utf-8 -*-
import sys
import os
import logging
import xlwt
import datetime
from sys import exit
import requests
import requests.certs
import yaml
from multiprocessing import Queue
import zipfile
import shutil
import re

if getattr(sys, 'frozen', False):
    # frozen
    dir_ = os.path.dirname(sys.executable)
else:
    # unfrozen
    dir_ = os.path.dirname(os.path.realpath(__file__))

__location__ = os.path.realpath(os.path.join(os.getcwd(), dir_, os.pardir))
logging.basicConfig(level=logging.INFO, filename=os.path.join(__location__, 'log', "log.log"), format='%(asctime)s :: %(levelname)s :: %(message)s')

def printe(t):
    print(t)
    exit()

def log(text, type='info'):
    if (type == 'info'):
        logging.info(text)
    elif (type == 'warning'):
        logging.warning(text)
    elif (type == 'error'):
        logging.error(text)
    elif (type == 'critical'):
        logging.critical(text)

    if config['DEBUG_MODE']:
        print(text)


def zipdir(path, ziph):
    for root, dirs, files in os.walk(path):
        for file in files:
            ziph.write(os.path.join(root, file))


class Main:
    excel_path = os.path.join(__location__, 'import', 'pedidos.xls')
    excel_path_business = os.path.join(__location__, 'import', 'empresas.xls')
    excel_path_backup = os.path.join(__location__, 'import', 'backup')

    def __init__(self, MercadoLibre, config, Logging):
        log('Se inicio el proceso de sincronizacion.')

        # Trae configuraciones del archivo config.yml
        self.config = config

        # Carga el certificado para requests
        if os.path.isfile(os.path.join(__location__, "script", "cacert.pem")):
            cert = os.path.join(__location__, "script", "cacert.pem")
        else:
            cert = requests.certs.where()

        # inicializa mercadolibre
        self.mercadolibre = MercadoLibre(str(self.config['APP_ID']), str(self.config['APP_SECRET']), cert)
        log('Se genero el token: {0}'.format(self.mercadolibre.access_token))

        # Setea la query para traer los productos
        self.setProductsQuery()

        # Aca se obtienen los datos de la db
        self.database = DataBase(config)
        if not self.database.test_connection():
            log('Error en la base de datos: {0}'.format(self.database.error))
            exit(-1)
        else:
            self.database.connect()

        # Checkea estado del servicio contra el server
        self.validServiceStatus()

        # Trae los productos de tactica
        self.products = self.database.query(self.QUERY)

        # Revisa por si ya tiene un token creado
        self.check_token_file()

    # Methodo de prueba del programa
    # Evalua si tiene servicio activo, si las credenciales de mercadolibre son correctas,  que se haya obtenido el access token.
    def test(self):
        print('Modo de prueba de base de datos y mercadolibre')
        status = self.mercadolibre.check_session()
        if status and not self.mercadolibre.error and self.mercadolibre.access_token and self.mercadolibre.refresh_token:
            print("TODAS LAS HERRAMIENTAS FUNCIOAN CORRECTAMENTE")
        else:
            print("¡¡¡SE ENCONTRARON ERRORES EN EL PROCESO!!!")
        print('Error de mercadolibre: {0} - {1}'.format(self.mercadolibre.error, self.mercadolibre.message))
        print('Access token de mercadolibre: {0}'.format(self.mercadolibre.access_token))
        print('Refresh token de mercadolibre: {0}'.format(self.mercadolibre.refresh_token))
        print('Resultados de la base de datos: {0}'.format(len(self.products)))
        input('\r\nPrecione enter para terminar...')

    # Metodo que procesa los productos de tactica para determinar la accion a tomar
    def process_products(self):
        log('--------------------------------------------------------------')
        log("Inicia la sincronizacion de stock y precio")
        log('--------------------------------------------------------------')
        not_found = 0
        updatedProducts = 0
        self.productos_repetidos = []

        # Verifica si hay algun cambio
        for product in self.products:
            if not product['ProductoCodigoML']:
                not_found += 1
                continue
            if product['ProductoCodigoML'] in self.productos_repetidos:
                log('El producto {0} esta repetido y no se tomara.')
                continue

            ml_codes = self.format_code(product['ProductoCodigoML'])

            if not ml_codes:
                continue

            product['ProductoCodigoML'] = ml_codes[0]
            if len(ml_codes) > 1:
                product['Variante'] = ml_codes[1]
            else:
                product['Variante'] = False

            # Trae el producto de mercadolibre
            item = self.mercadolibre.get_product(product['ProductoCodigoML'])

            if not item:
                log('no se encontro el producto: {0}'.format(self.mercadolibre.error))
                log('producto {0} con codigo ml {1} no encontrado'.format(
                    product['Codigo'],
                    product['ProductoCodigoML']
                ), 'error')
                continue

            if item['status'] and item['status'] in [401, 404]:
                log(item['message'])
                continue

            # si es variacion lo trabaja como tal, sino lo trabaja como unico
            if product['Variante']:
                log("Revisando producto variante {0}".format(product['Codigo']))
                updated = self.update_variant(product, item)

            else:
                log("Revisando producto comun {0}".format(product['Codigo']))
                updated = self.update_product(product, item)

            if updated:
                updatedProducts += 1

        # For end

        log('Se encontraron {0} productos para comprobar.'.format(len(self.products)))
        if not_found > 0:
            log("{0} productos no se encontro codigo de mercadolibre.".format(not_found))

        if updatedProducts > 0:
            log(logging.ERROR, "{0} productos actualizados.".format(updatedProducts))


    # Actualiza producto en mercadolibre de tipo variante
    def update_variant(self, product, item):
        variant = self.mercadolibre.get_product_variation(product['ProductoCodigoML'], product['Variante'])

        # Actualiza precio
        if variant['price'] != product['ProductoPrecioVentaPesosLista1'] and product[
            'ProductoPrecioVentaPesosLista1'] > 0:
            res = self.mercadolibre.change_price(product['ProductoCodigoML'], product['ProductoPrecioVentaPesosLista1'],
                                                 variant['id'])

            # Sube los cambios si es que existen
            if res:
                updated = True
                log('Producto {0} actualizado precio'.format(product['Codigo']))
                self.productos_repetidos.append(product['ProductoCodigoML'])
            else:
                # Si no deja, intentamos con el precio padre.
                last_error = self.mercadolibre.error
                res2 = self.mercadolibre.change_price(product['ProductoCodigoML'],
                                                      product['ProductoPrecioVentaPesosLista1'], item['variations'])

                if res2:
                    updated = True
                    log('Producto {0} actualizado precio padre'.format(product['Codigo']))
                    self.productos_repetidos.append(product['ProductoCodigoML'])
                else:
                    log(self.mercadolibre.error)

        # Actualiza stock
        if variant['available_quantity'] != product['ProductoStockSumaCantidadActualDisponibleNoReservado']:
            res = self.mercadolibre.change_stock(product['ProductoCodigoML'],
                                                 product['ProductoStockSumaCantidadActualDisponibleNoReservado'],
                                                 variant['id'])

            # Sube los cambios si es que existen
            if res:
                log('Producto {0} actualizado stock'.format(product['Codigo']))
                updated = True
                self.productos_repetidos.append(product['ProductoCodigoML'])
            else:
                log('error de mercadolibre: '+self.mercadolibre.error)

    # Actualiza producto en mercadolibre de tipo comun
    def update_product(self, product, item):
        updated = False

        if item['price'] != product['ProductoPrecioVentaPesosLista1'] and product['ProductoPrecioVentaPesosLista1'] > 0:
            res = self.mercadolibre.change_price(product['ProductoCodigoML'], product['ProductoPrecioVentaPesosLista1'])

            # Sube los cambios si es que existen
            if res:
                updated = True
                log('Producto {0} actualizado precio'.format(product['Codigo']))
                self.productos_repetidos.append(product['ProductoCodigoML'])
            else:
                log(self.mercadolibre.message)

        if product['UsaStock'] and (item['status'] == 'paused' or item['available_quantity'] != product[
            'ProductoStockSumaCantidadActualDisponibleNoReservado']):
            res = self.mercadolibre.change_stock(product['ProductoCodigoML'],
                                                 product['ProductoStockSumaCantidadActualDisponibleNoReservado'])
            # Sube los cambios si es que existen
            if res:
                log('Producto {0} actualizado stock'.format(product['Codigo']))
                self.productos_repetidos.append(product['ProductoCodigoML'])
                return True
            else:
                log("Error de mercadolibre en producto {0}: {1}".format(product['Codigo'], self.mercadolibre.message))
                return False

        return updated

    # Obtiene las ordenes filtradas de mercadolibre, las procesa, exporta las empresas y los pedidos que pasaron las validaciones.
    def sync_orders(self):
        orders = self.mercadolibre.get_orders()

        success_orders = self.filter_orders(orders)

        # Se exporta el excel para crear la empresa
        # TODO: Falta pasar las empresas a ser importadas.
        companies = self.export_business_excel(success_orders)

        # Se exporta a excel de pedido
        self.export_orders_excel(success_orders, companies)

    # Exporta las ordenes a un archivo excel para su importacion
    def export_orders_excel(self, success_orders, companies):
        log('--------------------------------------------------------------')
        log("Inicia la exportacion de pedidos por {0} ordenes".format(len(success_orders)))
        log('--------------------------------------------------------------')
        wb = xlwt.Workbook()
        ws = wb.add_sheet("WS 1")

        # Se setean las columnas
        ws.write(0, 0, '-')
        ws.write(0, 1, 'PEDIDOOEMPRESASISTEMAFISCAL')
        ws.write(0, 2, 'PEDIDOOTALONARIOSUCURSAL')
        ws.write(0, 3, 'PEDIDONOMBRE')
        ws.write(0, 4, 'PEDIDOORIGEN')
        ws.write(0, 5, 'PEDIDOOEMPRESACLIENTENOMBRE')
        ws.write(0, 6, 'PEDIDOOEMPRESACLIENTEFISCALRAZONSOCIAL')
        ws.write(0, 7, 'PEDIDOCONTACTONOMBRE')
        ws.write(0, 8, 'PEDIDOCONTACTOAPELLIDO')
        ws.write(0, 9, 'PEDIDOSUMARIMPUESTOS')
        ws.write(0, 10, 'PEDIDOMONEDANUMERO')
        ws.write(0, 11, 'PEDIDODESCRIPCION')
        ws.write(0, 12, 'PEDIDOTRANSPORTE')
        ws.write(0, 13, 'PEDIDOITEMNUMERO')
        ws.write(0, 14, 'PEDIDOITEMPRODUCTOCODIGO')
        ws.write(0, 15, 'PEDIDOITEMPRODUCTOCANTIDAD')
        ws.write(0, 16, 'PEDIDOITEMPRODUCTOPRECIOUNITARIO')
        ws.write(0, 17, 'PEDIDOITEMPRODUCTOIMPUESTO')

        row = 1
        for order in success_orders:

            # Selecciona la empresa correspondiente
            company = {}
            if companies:
                for comp in companies:
                    if comp['order_id'] != order['id']:
                        continue
                    company = {
                        'EMPRESA_SISTEMA_FISCAL': comp['nickname'],
                        'EMPRESA_CLIENTE_NOMBRE': comp['nickname'],
                        'EMPRESA_CLIENTE_FISCAL_RAZON_SOCIAL': comp['nickname'],
                        'CONTACTO_NOMBRE': comp['first_name'],
                        'CONTACTO_APELLIDO': comp['last_name'],
                    }
                    break
                if not company:
                    company = {
                        'EMPRESA_SISTEMA_FISCAL': self.config['EMPRESA_SISTEMA_FISCAL'],
                        'EMPRESA_CLIENTE_NOMBRE': self.config['EMPRESA_CLIENTE_NOMBRE'],
                        'EMPRESA_CLIENTE_FISCAL_RAZON_SOCIAL': self.config['EMPRESA_CLIENTE_FISCAL_RAZON_SOCIAL'],
                        'CONTACTO_NOMBRE': self.config['CONTACTO_NOMBRE'],
                        'CONTACTO_APELLIDO': self.config['CONTACTO_APELLIDO'],
                    }
            else:
                company = {
                    'EMPRESA_SISTEMA_FISCAL': self.config['EMPRESA_SISTEMA_FISCAL'],
                    'EMPRESA_CLIENTE_NOMBRE': self.config['EMPRESA_CLIENTE_NOMBRE'],
                    'EMPRESA_CLIENTE_FISCAL_RAZON_SOCIAL': self.config['EMPRESA_CLIENTE_FISCAL_RAZON_SOCIAL'],
                    'CONTACTO_NOMBRE': self.config['CONTACTO_NOMBRE'],
                    'CONTACTO_APELLIDO': self.config['CONTACTO_APELLIDO'],
                }

            item_number = 1
            first = True
            total = 0
            for item in order['items']:
                total += item['unit_price'] * item['quantity']

            for item in order['items']:
                # Revisa si el producto es un kit o un producto normal
                if item['variation']:
                    kit = self.database.query(
                        """SELECT 1 FROM productos P RIGHT JOIN productosinsumos PI ON PI.idproducto = P.recid WHERE P.CodElectronico LIKE '%{0}%' AND P.CodElectronico LIKE '%variation={1}%' LIMIT 1""".format(
                            item['id'], item['variation']))
                else:
                    kit = self.database.query(
                        """SELECT 1 FROM productos P RIGHT JOIN productosinsumos PI ON PI.idproducto = P.recid WHERE P.CodElectronico LIKE '%{0}%' LIMIT 1""".format(
                            item['id']))

                is_kit = True if len(kit) > 0 else False

                if is_kit:
                    log("producto tipo kit")
                    # BUSCA EN LA DB LOS PRODUCTOS DEL KIT (ARMADO)
                    if_variation = ' AND P.CodElectronico LIKE "%variation={0}%"'.format(item['variation']) if item[
                        'variation'] else ''
                    query = """
                        SELECT 
                            P.Codigo,
                            P2.Codigo AS CodigoInsumo,
                            P2.CodElectronico AS ProductoCodigoML,
                            PI.cantidad as ProdKitInsumoCantidad,
                            SUM(CASE psm.tipo 
                                WHEN 0 THEN psm.Cantidad*psm.Equivalencia 
                                WHEN 1 THEN psm.Cantidad*psm.Equivalencia*-1 
                                WHEN 2 THEN psm.Cantidad*psm.Equivalencia*-1 
                                ELSE 0 END) AS ProductoStockSumaCantidadActualDisponibleNoReservado,
                            1 AS UsaStock
                        FROM productos P 
                        RIGHT JOIN productosinsumos PI ON PI.idproducto = P.recid 
                        LEFT JOIN productos P2 ON P2.RecID = PI.IDProductoInsumo
                        LEFT JOIN productosstockmovimientos psm ON psm.idproducto = P2.recid
                        WHERE P.CodElectronico LIKE '%{0}%'
                        {1}
                        GROUP BY P2.recid;
                        """.format(
                        item['id'],
                        if_variation
                    )

                    kit = self.database.query(query)
                    # VALIDA SI TIENE PRODUCTOS HIJOS
                    if len(kit) == 0:
                        log('Producto kit {0} no encontrado.'.format(item['id']), 'error')
                        continue

                    # RECORRE LOS PRODUCTOS HIJOS Y POR CADA UNO PIDE ACTUALIZAR SU STOCK EN MERCADOLIBRE
                    for insumo in kit:
                        ml_codes = self.format_code(insumo['ProductoCodigoML'])
                        if not ml_codes:
                            ml_codes = [False]
                        if insumo['ProductoStockSumaCantidadActualDisponibleNoReservado'] > 0 and ml_codes[0]:
                            # Primero lo envia a mercadolibre
                            new_stock = insumo['ProductoStockSumaCantidadActualDisponibleNoReservado'] - (
                                        insumo['ProdKitInsumoCantidad'] * item['quantity'])
                            if len(ml_codes) > 1:
                                self.mercadolibre.change_stock(ml_codes[0], new_stock, ml_codes[1])
                            else:
                                self.mercadolibre.change_stock(ml_codes[0], new_stock)
                        else:
                            new_stock = 0

                        # EN CASO DE QUE UN PRODUCTO HIJO SE QUEDE SIN STOCK DAR EL KIT DE BAJA EN MERCADOLIBRE
                        if new_stock < insumo['ProdKitInsumoCantidad'] and insumo['UsaStock']:
                            self.mercadolibre.pause_publication(ml_codes[0])
                    # --for end--

                    # Luego lo exporta al excel para importar el pedido
                    ws.write(row, 0, '-')
                    ws.write(row, 1, company['EMPRESA_SISTEMA_FISCAL'])  # Razon social
                    ws.write(row, 2, '0002')
                    ws.write(row, 3, order['id'])
                    ws.write(row, 4, 'MERCADO-LIBRE')
                    ws.write(row, 5, company['EMPRESA_CLIENTE_NOMBRE'])
                    ws.write(row, 6, company['EMPRESA_CLIENTE_FISCAL_RAZON_SOCIAL'])  # razon social real
                    ws.write(row, 7, company['CONTACTO_NOMBRE'])
                    ws.write(row, 8, company['CONTACTO_APELLIDO'])
                    ws.write(row, 9, '1')
                    ws.write(row, 10, '1')
                    ws.write(row, 11, 'Pedido por mercadolibre')
                    ws.write(row, 12, order['shipping'])
                    ws.write(row, 13, item_number)
                    ws.write(row, 14, self._parse_numeric(kit[0]['Codigo']))
                    ws.write(row, 15, item['quantity'])
                    ws.write(row, 16, item['unit_price'])
                    ws.write(row, 17, '21')
                else:
                    log("producto tipo normal")
                    # Trae el producto de la db de tactica
                    query = "SELECT Codigo FROM productos WHERE CodElectronico LIKE '%{0}%'".format(item['id'])
                    product = self.database.query(query)

                    if not product:
                        log('El producto con codigo mercadolibre {0} No existe en la base de datos de tactica.'.format(
                                        str(item['id'])),'error'
                        )
                        continue

                    ws.write(row, 0, '-')
                    ws.write(row, 1, company['EMPRESA_SISTEMA_FISCAL'])  # Razon social
                    ws.write(row, 2, '0002')
                    ws.write(row, 3, order['id'])
                    ws.write(row, 4, 'MERCADO-LIBRE')
                    ws.write(row, 5, company['EMPRESA_CLIENTE_NOMBRE'])
                    ws.write(row, 6, company['EMPRESA_CLIENTE_FISCAL_RAZON_SOCIAL'])  # razon social real
                    ws.write(row, 7, company['CONTACTO_NOMBRE'])
                    ws.write(row, 8, company['CONTACTO_APELLIDO'])
                    ws.write(row, 9, '1')
                    ws.write(row, 10, '1')
                    ws.write(row, 11, 'Pedido por mercadolibre')
                    ws.write(row, 12, order['shipping'])
                    ws.write(row, 13, item_number)
                    ws.write(row, 14, self._parse_numeric(product[0]['Codigo']))
                    ws.write(row, 15, '0.1' if not first else total)
                    ws.write(row, 16, '0.1')
                    ws.write(row, 17, '21')

                item_number += 1
                row += 1
                first = False

            row += 2

        wb.save(self.excel_path)
        now = datetime.datetime.now()
        zip_file = 'IMPORT-PEDIDO_{0}.zip'.format(now.strftime('%Y%m%d-%H%M%S'))
        zipf = zipfile.ZipFile(os.path.join(self.excel_path_backup, zip_file), 'w', zipfile.ZIP_DEFLATED)
        zipdir(self.excel_path, zipf)

    # Exporta las empresas a un archivo excel para su importacion
    def export_business_excel(self, success_orders):
        if not self.config['USE_DYNAMIC_COMPANY']:
            if os.path.isfile(self.excel_path_business):
                os.remove(self.excel_path_business)
            log('--------------------------------------------------------------')
            log("Exportacion de empresas dinamicas desactivada")
            log('--------------------------------------------------------------')
            return
        log('--------------------------------------------------------------')
        log("Inicia el proceso de exportacion de empresas.")
        log('--------------------------------------------------------------')
        # Empresas a exportar
        companies = []
        new_companies = []
        for order in success_orders:
            order_already_add = order['buyer']['id'] in [d.get('id') for d in companies]
            if not order_already_add and not self.check_if_company_exist(order['buyer']['nickname']):
                company = order['buyer']
                company['order_id'] = order['id']
                companies.append(company)
                new_companies.append(company)
            else:
                company = order['buyer']
                company['order_id'] = order['id']
                companies.append(company)

        wb = xlwt.Workbook()
        ws = wb.add_sheet("S1")
        now = datetime.datetime.now()

        # Se setean las columnas
        ws.write(0, 0, '-')
        ws.write(0, 1, 'EMPRESANOMBRE')
        ws.write(0, 2, 'EMPRESAFUENTE')
        ws.write(0, 3, 'EMPRESADIRPAIS')
        ws.write(0, 4, 'EMPRESADIRPROVINCIA')
        ws.write(0, 5, 'EMPRESADIRLOCALIDAD')
        ws.write(0, 6, 'EMPRESADIRCODPOSTAL')
        ws.write(0, 7, 'EMPRESADIRCALLENOMBRE')
        ws.write(0, 8, 'EMPRESADIRCALLENRO')
        ws.write(0, 9, 'EMPRESADIRPISO')
        ws.write(0, 10, 'EMPRESADIRDPTO')
        ws.write(0, 11, 'EMPRESATELEFONO1TIPO')
        ws.write(0, 12, 'EMPRESATELEFONO1NRO')
        ws.write(0, 13, 'FISCALRAZONSOCIAL')
        ws.write(0, 14, 'FISCALTIPOIMPUESTO')
        ws.write(0, 15, 'FISCALCUIT')
        ws.write(0, 16, 'FISCALMODALIDADCLIENTE')
        ws.write(0, 17, 'FISCALMODALIDADPROVEEDOR')
        ws.write(0, 18, 'CONTACTOAPELLIDO')
        ws.write(0, 19, 'CONTACTONOMBRE')
        ws.write(0, 20, 'CONTACTOCORREO')
        ws.write(0, 21, 'CONTACTOTELEFONOTIPO')
        ws.write(0, 22, 'CONTACTOTELEFONONRO')
        ws.write(0, 23, 'ACTIVIDADFECHA')
        ws.write(0, 24, 'ACTIVIDADHORA')
        ws.write(0, 25, 'ACTIVIDADDURACION')
        ws.write(0, 26, 'ACTIVIDADNOTAS')
        ws.write(0, 27, 'ACTIVIDADREFERENCIA')
        ws.write(0, 28, 'ACTIVIDADTIPO')
        ws.write(0, 29, 'ACTIVIDADUSUARIO')
        ws.write(0, 30, 'ACTIVIDADPRIORIDAD')
        ws.write(0, 31, 'ACTIVIDADCONHORA')
        ws.write(0, 32, 'ACTIVIDADFECHAHORAALARMA')

        # ACA VA LA IMPORTACION DE LA EMPRESA COMO TAL
        row = 1
        for company in new_companies:
            # Por cada compania trae la informacion.
            # user_info = self.mercadolibre.get_user_informaton(company['id'])

            fiscal_cuit = company['billing_info']['doc_number'] if company['billing_info']['doc_number'] else \
            self.config['DEFAULT_CUIT']
            is_business = '1' if company['billing_info']['doc_number'] else '7'

            ws.write(row, 0, '-')
            ws.write(row, 1, company['nickname'])  # EMPRESANOMBRE
            ws.write(row, 2, 'MERCADO-LIBRE')  # EMPRESAFUENTE
            ws.write(row, 3, '-')  # EMPRESADIRPAIS
            ws.write(row, 4, '-')  # EMPRESADIRPROVINCIA
            ws.write(row, 5, '-')  # EMPRESADIRLOCALIDAD
            ws.write(row, 6, '-')  # EMPRESADIRCODPOSTAL
            ws.write(row, 7, '-')  # EMPRESADIRCALLENOMBRE
            ws.write(row, 8, '-')  # EMPRESADIRCALLENRO
            ws.write(row, 9, '-')  # EMPRESADIRPISO
            ws.write(row, 10, '-')  # EMPRESADIRDPTO
            ws.write(row, 11, '5')  # EMPRESATELEFONO1TIPO # TODO: Asi venia en el excel, luego consultar
            ws.write(row, 12, company['phone']['number'])  # EMPRESATELEFONO1NRO
            ws.write(row, 13, company['nickname'])  # FISCALRAZONSOCIAL
            ws.write(row, 14, '54000')  # FISCALTIPOIMPUESTO # TODO: Asi venia en el excel, luego consultar
            ws.write(row, 15, fiscal_cuit)  # FISCALCUIT
            ws.write(row, 16, is_business)  # TODO: 1 si es empresa sino 7           #FISCALMODALIDADCLIENTE
            ws.write(row, 17, is_business)  # TODO: 1 si es empresa sino 7           #FISCALMODALIDADPROVEEDOR
            ws.write(row, 18, company['last_name'])  # CONTACTOAPELLIDO
            ws.write(row, 19, company['first_name'])  # CONTACTONOMBRE
            ws.write(row, 20, company['email'])  # CONTACTOCORREO
            ws.write(row, 21, '0')  # CONTACTOTELEFONOTIPO
            ws.write(row, 22, company['phone']['number'])  # CONTACTOTELEFONONRO
            ws.write(row, 23, now.strftime("%Y-%m-%d"))  # ACTIVIDADFECHA
            ws.write(row, 24, now.strftime("%H:%M"))  # ACTIVIDADHORA
            ws.write(row, 25, '5')  # ACTIVIDADDURACION
            ws.write(row, 26, 'COMPRA DE MERCADOLIBRE - PREPARAR PEDIDO, IMPRIMIR ETIQUETA')  # ACTIVIDADNOTAS
            ws.write(row, 27, 'COMPRA MERCADOLIBRE')  # ACTIVIDADREFERENCIA
            ws.write(row, 28, 'ACCIÓN')  # ACTIVIDADTIPO
            ws.write(row, 29, 'ADMINISTRADOR')  # ACTIVIDADUSUARIO
            ws.write(row, 30, '1')  # ACTIVIDADPRIORIDAD
            ws.write(row, 31, '1')  # ACTIVIDADCONHORA
            ws.write(row, 32, now.strftime("%Y-%m-%d %H:%M"))  # ACTIVIDADFECHAHORAALARMA

            row += 1

        wb.save(self.excel_path_business)
        now = datetime.datetime.now()
        zip_file = 'IMPORT-EMPRESA_{0}.zip'.format(now.strftime('%Y%m%d-%H%M%S'))
        zipf = zipfile.ZipFile(os.path.join(self.excel_path_backup, zip_file), 'w', zipfile.ZIP_DEFLATED)
        zipdir(self.excel_path_business, zipf)

        return companies

    # Filtra ordenes segun definidas reglas y devuelve un objeto formateado
    def filter_orders(self, orders):

        success_orders = []
        if not orders:
            log('no se pueden procesar las ordenes')
            return success_orders
        for order in orders:
            # Filtrar ordenes primero por las completas luego por los pedidos ya recibidos que son guardados en cada la base de datos o el excel

            if self.check_if_order_exist(order['id']) or order['status'] != 'paid':
                continue

            items = []
            for item in order['order_items']:
                variation = False
                if item['item']['variation_id']:
                    ml_id = item['item']['id']
                    variation = item['item']['variation_id']
                else:
                    ml_id = item['item']['id'].replace('MLA', 'MLA-')

                items.append({
                    'id': ml_id,
                    'variation': variation,
                    'unit_price': item['unit_price'],
                    'quantity': item['quantity'],
                })

            # Se crea un array con los un array por cada pedido listo para exportar a excel
            shipping = order['shipping']['shipping_mode'] if order['shipping']['shipping_mode'] else 'Retiro'
            success_orders.append({
                'id': order['id'],
                'comments': order['comments'],
                'buyer': order['buyer'],
                'shipping': shipping,
                'total_amount': order['total_amount'],
                'items': items
            })

        return success_orders

    # Revisa si el pedido existe en tactica
    def check_if_order_exist(self, id):
        query = 'SELECT 1 FROM pedidos WHERE Nombre = "{0}" LIMIT 1;'.format(id)
        result = self.database.query(query)
        return result

    # Revisa si la empresa existe en tactica
    def check_if_company_exist(self, name):
        query = 'SELECT 1 FROM empresas WHERE Empresa = "{0}" LIMIT 1;'.format(name)
        result = self.database.query(query)
        return len(result) > 0

    # Busca el token guardado, en caso de tenerlo lo usa, sino pide uno nuevo y lo guarda
    def check_token_file(self):
        if os.path.isfile(__location__ + "token"):
            file = open(__location__ + "token", "r")
            token = file.readline()
            if token:
                self.mercadolibre.refresh_token = str(token)
                if self.mercadolibre.refresh_access_token():
                    return True

        self.mercadolibre.generate_access_token()
        file = open(os.path.join(__location__, "token"), "w")
        file.write(str(self.mercadolibre.refresh_token))
        file.close()

    # Formatea la url del producto de mercadolibre a los codigos necesarios para procesar
    def format_code(self, product):
        if not product:
            return False
        # Parceo simple para codigo de mercadolibre cuando se copia por link
        regex_ml = r"MLA-\d{1,20}"
        regex_variante = r"variation=(\d{1,20})"
        ml_code = re.findall(regex_ml, product)
        variant = re.findall(regex_variante, product)
        if len(ml_code) < 1:
            log('No se encontro el codigo MLA en el link: {0}'.format(product), 'error')
            return False

        result = [ml_code[0].replace('-', '')]
        if len(variant) > 0:
            result.append(variant[0])

        return result

    # Revisa todos los kit de tactica y actualiza la informacion en mercadolibre
    def update_kits_ml(self):
        log('--------------------------------------------------------------')
        log("Inicia proceso de actualizacion de kits")
        log('--------------------------------------------------------------')
        kits = self.database.query(self.kitsQuery)

        if len(kits) == 0:
            log("No se encontraron kits para procesar.")
            return

        group_kits = {}
        # Agrupa los kits para trabajarlos mejor
        for kit in kits:
            if kit['Codigo'] not in group_kits:
                group_kits[kit['Codigo']] = []

            group_kits[kit['Codigo']].append(kit)

        for [codigo, kit] in group_kits.items():
            ml_codes = self.format_code(kit[0]['ProductoCodigoKitML'])
            if not ml_codes:
                continue
            kit_stock = 999  # Maximo stock para reducir a medida que revisa los insumos
            ml_product = self.mercadolibre.get_product(ml_codes[0])
            if not ml_product:
                log('producto {0} ({1}) no existe en mercadolibre'.format(codigo, ml_codes[0]))
                continue

            every_item_has_stock = True
            log('\r\n///////////////////\r\n')
            log('Kit {0}'.format(codigo))
            for insumo in kit:
                log('\r\n')
                if not insumo['UsaStock']:
                    log('insumo no usa stock')
                    continue
                log('insumo {0}'.format(insumo['CodigoInsumo']))
                log('insumo usa stock? {0}'.format(insumo['UsaStock']))
                log('insumo stock: {0}'.format(insumo['InsumoStock']))
                log('Producto pide {0}'.format(insumo['ProdKitInsumoCantidad']))
                if insumo['InsumoStock'] < insumo['ProdKitInsumoCantidad']:
                    log('{0} no tiene stock'.format(insumo['CodigoInsumo']))
                    every_item_has_stock = False
                    break

                if insumo['InsumoStock'] >= insumo['ProdKitInsumoCantidad']:
                    every_item_has_stock = True
                    log('Insumo tiene stock')
                    # si es mayor quiere decir que no se debe setear, el minimo stock es el del kit.
                    if kit_stock > int(insumo['InsumoStock'] / insumo['ProdKitInsumoCantidad']):
                        kit_stock = int(insumo['InsumoStock'] / insumo['ProdKitInsumoCantidad'])
                        log('ultimo stock: {0}'.format(kit_stock))

            if every_item_has_stock:
                log('Stock del kit: {0}'.format(kit_stock))
                if ml_product['status'] in ['paused', 'closed', 'inactive', 'not_yet_active']:
                    self.mercadolibre.resume_publication(ml_codes[0])
                    ml_product['status'] = 'active'

                if kit_stock != ml_product['available_quantity']:
                    self.mercadolibre.change_stock(ml_codes[0], kit_stock)
                    log('{0} paso a estado activo con nuevo stock {1}'.format(codigo, kit_stock))
            else:
                if ml_product['status'] != 'paused':
                    self.mercadolibre.pause_publication(ml_codes[0])
                    ml_product['status'] = 'paused'

        log('--------------------------------------------------------------')
        log('Finalizo actualizacion de kits')
        log('--------------------------------------------------------------')

    def _parse_numeric(self, code):
        if code[0] == 0:
            return '\'' + str(code)
        else:
            return str(code)

    # Revisa si tiene un servicio activo con las credenciales configuradas
    def validServiceStatus(self):
        headers = {"AuthorizationBusiness": self.config['WS_ACCESS_TOKEN']}
        url = self.config['WS_HOST'] + ':' + str(self.config['WS_PORT']) + self.config['WS_PREFIX'] + 'sync'

        response = requests.get(url, headers=headers)

        if response.status_code == 401:
            if 'message' in response.json() and response.json()['message']:
                raise Exception(response.json()['message'])
            else:
                raise Exception("Error al ejecutar la request")
        elif response.status_code == 200:
            try:
                if 'service_status' in response.json() and response.json()['service_status']:
                    log("--------------------------------------------")
                    log('Servicio valido.')
                    log("--------------------------------------------")
                    return True
            except:
                if 'success' not in response.json() or response.json()[
                    'success'] or 'service_status' not in response.json() or not response.json()['service_status']:
                    if 'msg' in response.json():
                        raise Exception(response.json()['msg'])
                    else:
                        raise Exception('Servicio inactivo')
            # END IF
        elif response.status_code == 500:
            try:
                log(response.json()['error']['msg'])
                raise Exception('ERROR AL CORRER LA REQUEST')
            except:
                raise Exception('ERROR INTERNO DEL SERVICIO.')
        else:
            log(response.content)
            raise Exception('ERROR DESCONOCIDO AL CORRER LA REQUEST')
        # END IF

    # Genera un usuario de prueba y lo almacena en un archivo usuarios.ml
    def new_user_test(self):
        # for i in self.TEST_USERS:
        log('Generando usuario de test')
        log('-----------------')
        user = self.mercadolibre.generate_test_user()
        if not user:
            log('Se encontro un problema al generar el usuario.')
            log(self.mercadolibre.error)
            log(self.mercadolibre.message)
            return

        file = open(os.path.join(__location__, "usuarios.ml"), "a")
        file.write('\r\n----------------------------')
        file.write('\r\nId:          {0}'.format(user['id']))
        file.write('\r\nEmail:       {0}'.format(user['email']))
        file.write('\r\nPassword:    {0}'.format(user['password']))
        file.write('\r\nNickname:    {0}'.format(user['nickname']))
        file.write('\r\nSite status: {0}'.format(user['site_status']))
        file.write('\r\n----------------------------\r\n')
        file.close()

        log('Id:          {0}'.format(user['id']))
        log('Email:       {0}'.format(user['email']))
        log('Password:    {0}'.format(user['password']))
        log('Nickname:    {0}'.format(user['nickname']))
        log('Site status: {0}'.format(user['site_status']))
        return

    def setProductsQuery(self):
        deposit = ' AND pd.Deposito = "' + config['DEPOSIT_NAME'] + '" ' if config['DEPOSIT_NAME'] else ''
        self.QUERY = """
                SELECT 
                    p.CodElectronico AS ProductoCodigoML, 
                    p.Codigo AS Codigo, 
                    pp.Precio AS ProductoPrecioVentaPesosLista1, 
                    SUM(CASE psm.tipo 
                        WHEN 0 THEN psm.Cantidad*psm.Equivalencia 
                        WHEN 1 THEN psm.Cantidad*psm.Equivalencia*-1 
                        WHEN 2 THEN psm.Cantidad*psm.Equivalencia*-1 
                        ELSE 0 END) AS ProductoStockSumaCantidadActualDisponibleNoReservado,
                    ControlaStock AS UsaStock
                FROM productos p

                LEFT JOIN productosstockmovimientos psm ON psm.idproducto = p.recid
                LEFT JOIN productosdepositos pd ON  psm.iddeposito = pd.RecID {deposit}
                LEFT JOIN productosprecios pp ON pp.IDProducto = p.RecID
                LEFT JOIN productosstock ps ON ps.IDProducto = p.RecID
                WHERE pp.nrolista = {list}
                AND p.CodElectronico IS NOT NULL
                AND p.CodElectronico <> '' 
                AND pp.NroMonedaPrecio = {currency}
                AND p.Estado = 0 
                AND p.Inhabilitado = 0
                GROUP BY psm.idproducto, p.CodElectronico, psm.iddeposito, pd.Deposito, pp.Precio, p.Codigo
                ORDER BY p.Codigo, pd.Deposito;
                """.format(
            list=config['NRO_LISTA'] if config['NRO_LISTA'] else 1,
            currency=config['NRO_MONEDA_PRECIO'] if config['NRO_MONEDA_PRECIO'] else 1,
            deposit=deposit
        )

        self.kitsQuery = """
        SELECT 
            P.Codigo,
            P.CodElectronico AS ProductoCodigoKitML,
            P2.Codigo AS CodigoInsumo,
            PI.cantidad as ProdKitInsumoCantidad,
            SUM(CASE psm.tipo 
                WHEN 0 THEN psm.Cantidad*psm.Equivalencia 
                WHEN 1 THEN psm.Cantidad*psm.Equivalencia*-1 
                WHEN 2 THEN psm.Cantidad*psm.Equivalencia*-1 
                ELSE 0 END) AS InsumoStock,
            ControlaStock AS UsaStock
        FROM productos P 
        RIGHT JOIN productosinsumos PI ON PI.idproducto = P.recid 
        LEFT JOIN productos P2 ON P2.RecID = PI.IDProductoInsumo
        LEFT JOIN productosstockmovimientos psm ON psm.idproducto = P2.recid
        LEFT JOIN productosdepositos pd ON  psm.iddeposito = pd.RecID {deposit}
        LEFT JOIN productosstock ps ON ps.idproducto = P2.recid
        WHERE P.CodElectronico IS NOT NULL 
            AND P.CodElectronico <> ''
            AND P.Estado = 0 
            AND P.Inhabilitado = 0
        GROUP BY P.Codigo, P2.Codigo
        ORDER BY P.Codigo, P2.Codigo;""".format(
            deposit=deposit
        )


if __name__ == "__main__":
    try:
        from mercadolibre_interface import MercadoLibre
        from database import DataBase

        # Carga de archivo de configuracion
        config = {}
        with open(os.path.join(__location__, "script", "config.yml"), 'r', encoding='utf8') as stream:
            try:
                config = yaml.load(stream)
            except yaml.YAMLError as exc:
                log(exc, 'error')
                exit(-1)

        main = Main(MercadoLibre, config, logging)

        if len(sys.argv) > 1 and sys.argv[1] == 'test':
            # Testeo del programa
            main.test()
            exit(1)
        elif len(sys.argv) > 1 and sys.argv[1] == 'orders':
            # Sincronizacion de ordenes unicamente
            main.sync_orders()
            exit(1)
        elif len(sys.argv) > 1 and sys.argv[1] == 'price-stock':
            # Sincronizacion de precio y stock unicamente
            main.process_products()
            exit(1)
        elif len(sys.argv) > 1 and sys.argv[1] == 'update-kits':
            # Sincronizacion de productos tipo kit unicamente
            main.update_kits_ml()
            exit(1)
        elif len(sys.argv) > 1 and sys.argv[1] == 'user-test':
            # Generacion de usuario de prueba unicamente
            main.new_user_test()
            input('...')
            exit(1)

        # Ejecuta el programa

        # Crea el excel de los pedidos para importar
        main.sync_orders()

        # Actualiza el estado de los kits
        main.update_kits_ml()

        # Sincroniza el stock y precio
        main.process_products()
    except Exception as e:
        log('Error fatal en el sistema: {0}'.format(str(e)), 'error')
        exit(-1)

    if len(sys.argv) > 1 and sys.argv[1] == 'manual':
        input('\r\nPrecione enter para finalizar...')
    exit(1)