import pymysql
#import mysql.connector

class DataBase:
    db = False
    error = False

    def __init__(self, config):
        self.config = config

    def query(self, query):
        cursor = self.db.cursor()
        #if self.use_pymsql:
        cursor.execute(query.encode('utf-8'))
        returnData = cursor.fetchall()
        cursor.close()
        #else:
        #    cursor.execute(query.encode('utf-8'))
        #    returnData = cursor.execute()
        #    cursor.close()
        return returnData

    def connect(self):
        #try:
        self.db = pymysql.connect(
            host =   str(self.config['DB_HOST']),
            port =   int(self.config['DB_PORT']),
            user =   str(self.config['DB_USER']),
            passwd = self.config['DB_PASSWORD'],
            db =     str(self.config['DB_DATABASE']),
            cursorclass=pymysql.cursors.DictCursor
        )
        self.use_pymsql = True
        #except:
        #    self.use_pymsql = False
        #    self.db = mysql.connector.connect(
        #        host = str(self.config['DB_HOST']),
        #        port = int(self.config['DB_PORT']),
        #        user = str(self.config['DB_USER']),
        #        password = str(self.config['DB_PASSWORD']) if self.config['DB_PASSWORD'] else False,
        #        database = str(self.config['DB_DATABASE']),
        #        auth_plugin='mysql_native_password'
        #    )



    def test_connection(self):
        try:
            self.connect()
            self.closeConnection()
            return True
        except Exception as e:
            self.error = e
            return False

    def closeConnection(self):
        self.db.close()